#!/bin/bash -e

# Update Yunohost WordPress App
# Version O.4
#
# Make executable: chmod u+x update_ynh_wordpress.sh
# Run as super user: 
#     _$: sudo -s
#     _$: ./update_ynh_wordpress.sh

# Note: force wp-cli to accept root command
function _clean_ldap {
    wp --allow-root plugin deactivate authldap
    wp --allow-root plugin uninstall authldap
}

function _clean_companion {
  wp --allow-root plugin deactivate companion-auto-update
  wp --allow-root plugin uninstall companion-auto-update
}

function _clean_all {
  wp --allow-root plugin deactivate authldap
  wp --allow-root plugin uninstall authldap
  wp --allow-root plugin deactivate companion-auto-update
  wp --allow-root plugin uninstall companion-auto-update
}

# A simple yes/no validation before we proceed
echo "Howdy! Do you really want to update WordPress? (y/n)"
read -e -r run

# if the user didn't say no, then go ahead and install
if [ "$run" == n ] ; then
  exit

else

  # Target 1
  echo 
  echo "Clean WordPress 1"
  echo "========================================================="

  # Move to app directory (needed for wp-cli)
  cd /var/www/wordpress/ || exit
  # Run update with Yunohost CLI
  yunohost app upgrade wordpress
  _clean_all  

  # Target 2
  echo 
  echo "Clean WordPress 2"
  echo "========================================================="

  cd /var/www/wordpress__2/ || exit
  # Run update with Yunohost CLI
  yunohost app upgrade wordpress__2
  _clean_companion 

  # Target 3
  echo 
  echo "Clean WordPress 3"
  echo "========================================================="

  cd /var/www/wordpress__3/ || exit
  # Run update with Yunohost CLI 
  yunohost app upgrade wordpress__3
  _clean_ldap 
  
  echo 
  echo "All jobs done! Have a nice day"
  echo "========================================================="

fi

Help() {
   echo
   echo "Script to upgrade RSS-Bridge on Yunohost"
   echo
   echo "- Upgrade application with Yunohost CLI"
   echo "- Options to deactivate `companion-auto-update` and/or `authldap` plugin"
   echo
   echo "Options:"
   echo "    h      Print this Help"
   echo "    l      Copyright & license"
   echo "    m      Political statement"
   echo
   echo "- Syntax:  [-h|l|m]"
   echo "- Example: ./update_ynh_wordpress.sh -h"
   echo "- Example: ./update_ynh_wordpress.sh -hlm"
   echo
}
Mascot() {
   echo
   echo "  Follow the White Rabbit"
   echo
   echo "  (\(\   "
   echo "  ( - -) "
   echo "  ((`)(`)"
   echo
}
license() {
   echo
   echo "This script is a free open source software."
   echo "Released in the public domain under Creative Commons CC0 License"
   echo
}

# Get the options
while getopts ":hlm" option; do
   case $option in
      h) 
         Help
         exit;;
      l) 
         License
         exit;;
      m) # Statement
         Mascot
         exit;;
     \?) # Ooooops!
         echo
         echo "Fatal error: invalid option!"
         echo "Look at the Help (-h) don't kill the white rabbit"
         echo
         exit;;
   esac
done
