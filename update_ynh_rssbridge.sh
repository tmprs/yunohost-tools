#!/bin/bash -e

# Update Yunohost RSS-Bridge App
# Version 0.5
#
# Make executable: chmod u+x update_ynh_rssbridge.sh
# Run as super user:
#     _$: sudo -s
#     _$: ./update_ynh_rssbridge.sh

# Configuration variables
ADMUSER=admin
DIRBASE=/home/
DIRPATH=/yunohost/rss-bridge/   # the source directory

# A simple yes/no validation before we proceed
echo "Howdy! Do you really want to upgrade RSS-Bridge? (y/n)"
read -e -r run

# If the user didn't say no, then go ahead and install
if [ "$run" == n ] ; then
  exit

else

  # Run package upgrade with Yunohost CLI
  yunohost app upgrade rss-bridge

  # Move to application directory
  # Note: Script will exit if there is no directory
  cd /var/www/rss-bridge/ || exit

  # Config File
  echo "Info: Restore custom config.ini.php file"
  rm -f config.ini.php
  #### cp -av /home/admin/yunohost/rss-bridge/config.ini.php /var/www/rss-bridge/
  cp -av ${DIRBASE}${ADMUSER}${DIRPATH}config.ini.php /var/www/rss-bridge/

  # Whitelist
  # Not needed anymore: done by Yunohost package now
  # Stay here for legacy
  #
  # echo "Info: Restore custom whitelist.txt file"
  # rm -f whitelist.txt
  # cp -av ${DIRBASE}${ADMUSER}${DIRPATH}whitelist.txt /var/www/rss-bridge/

  # Custom bridges
  echo "Info: Remove all default bridges"
  rm -fr bridges/
  echo "Info: Install only selected & custom bridges"
  #### cp -av  /home/admin/yunohost/rss-bridge/bridges/ /var/www/rss-bridge/
  cp -av  ${DIRBASE}${ADMUSER}${DIRPATH}bridges/ /var/www/rss-bridge/

  # Ownership and autorization
  echo "Info: Retore correct ownership on files and directory"
  chown -R rss-bridge:www-data bridges/ config.ini.php whitelist.txt

  echo "Info: Restore correct autorization for files"
  chmod 660 bridges/* config.ini.php whitelist.txt
  chmod 770 bridges/

  echo
  echo "Done! Enjoy free software…"

fi

Help() {
   echo
   echo "Script to upgrade RSS-Bridge on Yunohost"
   echo
   echo "- Upgrade application with Yunohost CLI"
   echo "- Restore your own config file from your backup"
   echo "- Restore custom bridges directory from backup"
   echo
   echo "For more information and instruction have a look here:"
   echo "https://codeberg.org/tmprs/yunohost-tools"
   echo
   echo "Options:"
   echo "    h      Print this Help"
   echo "    l      Copyright & license"
   echo "    m      Political statement"
   echo
   echo "- Syntax:  [-h|l|m]"
   echo "- Example: ./update_ynh_rssbridge.sh -h"
   echo "- Example: ./update_ynh_rssbridge.sh -hlm"
   echo
}
Mascot() {
   echo
   echo "  Follow the White Rabbit"
   echo
   echo "  (\(\   "
   echo "  ( - -) "
   echo "  ((`)(`)"
   echo
}
license() {
   echo
   echo "This script is a free open source software."
   echo "Released in the public domain under Creative Commons CC0 License"
   echo
}

# Get the options
while getopts ":hlm" option; do
   case $option in
      h)
         Help
         exit;;
      l)
         License
         exit;;
      m) # Statement
         Mascot
         exit;;
     \?) # Ooooops!
         echo
         echo "Fatal error: invalid option!"
         echo "Look at the Help (-h) don't kill the white rabbit"
         echo
         exit;;
   esac
done
