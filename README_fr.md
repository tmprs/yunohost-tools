# Yunohost Tools

Une collection d’outils pour la maintenance et la personnalisation d’applications YNH

### Fonctionnement

Les scripts proposées utilisent des commandes Yunohost _standards_, et ne modifient en aucun cas ni le système, ni les _packages_ d’applications. 

Schématiquement :

- Exécution de la commande Yunohost (en général `yunohost upgrade`).
- Eventuellement exécution d’une commande propre à l’application concernée (exemple `wpcli`).
- Si nécessaire manipulation de fichiers par des commandes shell de base (genre `rm`, `mv`,`chmod`).

### Avertissement

L’utilisation et la configuration de ces scripts supposent :

- Un minimun de connaissances du Shell Unix.
- Une compréhension minimale de ce qu’est une application (_package_) Yunohost.
- Une application stricte du principe RTFM (lire la documentation).

D’autre part, avoir en tête qu’il ne s’agit _pas_ de scripts à utiliser tels quels, mais plutôt d’une base à configurer et personnaliser en fonction de vos besoins et et de vos choix.

## Documentation

### update_ynh_rssbridge.sh

A chaque mise à jour le package RSS-Bridge de YNH, celui-ci de restaure _pas_ le fichier de configuration personnalisé de l’application… ce qui est assez pénible.

Par ailleurs nous voulions une possibilité de n’installer qu’une sélection restreinte de passerelles RSS.

**Configuration** 

Il faut renseigner deux informations comme prérequis à l’utilisation du script. 
Pour cela il faudra _modifier le fichier_ lui même (on vas essayer de faire mieux).

- L’identifiant de l’utilisateur « admin » que vous avez définit dans YNH (exemple : `supermario`).
- Le chemin d’accès aux fichiers qui doivent être restaurés (exemple : `/home/supermario/scripts/`)

**Installation**

Nous pouvons ensuit nous connecter au serveur en SFTP pour copier :

- Le fichier du script.
- La version de sauvegarde du fichier `config.ini.php`
- Le répertoire `bridges/` avec votre sélection de passerelles (_bridges_).

Pensez à rendre le script éxécutable : `sudo chmod u+x my_script_name.sh`

**Utilisation**

````
sudo -s ./update_ynh_rssbridge.sh
````

Attention :

- Ne pas procéder avant à la mise à jour via l’interface graphique d’admin de YNH.
- Ni lancer la mise à jour directement via la CLI de Yunohost.

### update_ynh_wordpress.sh

A chaque mise à jour le package WordPress de YNH installe et active deux plugins : Companion Auto Update et AuthLdap. Ceci sans prévenir ni demander de consentement.

Ce script permet de désactiver et désinstaller les deux plugins, ou l’un des deux (au choix), sur une ou plusieurs instances de WordPress.

**Configuration** 

Il faut modifier le code du script lui-même pour indiquer les différentes instances de WordPress concernées, et le type de mise à niveauappliqué. 

Par exemple pour mettre à jour la seconde instance WordPress :

````
  cd /var/www/wordpress__2/ || exit
  # Run update with Yunohost CLI
  yunohost app upgrade wordpress__2
  _clean_companion 
````

Trois fonctions peuvent être appliquées : `_clean_all`, `_clean_companion`et `_clean_ldap`.

**Installation**

Nous pouvons ensuit nous connecter au serveur en SFTP pour copier le fichier modifié.

Pensez à rendre le script éxécutable : `sudo chmod u+x my_script_name.sh`

**Utilisation**

````
sudo -s ./update_ynh_wordpress.sh
````

Attention :

- Ne pas procéder avant à la mise à jour via l’interface graphique d’admin de YNH.
- Ni lancer la mise à jour directement via la CLI de Yunohost.

_Enjoy free software!_
